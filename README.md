# 0800 Grove Pi Zero

![grovepi](img/image1.jpg)

## Objectifs

- Installer un GrovePi Zero
- Interagir avec ses capteurs et ses sorties
- connecter au serveur MQTT

## Matériel
- Poste de travail
- Kit GrovePi et Raspberry Zero W
- Logiciel MQTTX
- Logiciel Win32DiskImager
- Logiciel Putty
- Logiciel FileZilla

## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre <visa> est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom

## Introduction

Le Raspberry Pi Zero W est un mini-ordinateur monocarte très compact et économique, développé par la Fondation Raspberry Pi. Il est basé sur le même processeur Broadcom ARM11 que les autres modèles de Raspberry Pi, mais avec une configuration plus réduite. Le modèle "W" intègre également une connectivité sans fil Wi-Fi et Bluetooth, d'où son nom de "W" pour "Wireless".

Le Raspberry Pi Zero W dispose d'un processeur Broadcom BCM2835 cadencé à 1 GHz, d'une mémoire vive (RAM) de 512 Mo, d'un port mini-HDMI pour la sortie vidéo, d'un port micro-USB pour l'alimentation et la connexion au PC, d'un port micro-USB OTG, d'un connecteur pour une carte microSD, d'un port pour une caméra CSI, d'un port pour un écran tactile DSI, d'un port GPIO à 40 broches et d'un connecteur d'antenne pour la connectivité sans fil.

## Kit GrovePi Zero

En raison de sa taille compacte et de sa faible consommation d'énergie, le Raspberry Pi Zero W est souvent utilisé pour des projets embarqués, de l'IoT (Internet des Objets), de la robotique, de la domotique ou encore de l'audio. 

Le kit GrovePi Zero est un ensemble de capteurs et d'actionneurs pré-câblés, conçus pour être utilisés avec le mini-ordinateur monocarte Raspberry Pi Zero. Il est développé par la société Seeed Studio.

Le kit GrovePi Zero contient plusieurs capteurs et actionneurs tels que des capteurs de température, d'humidité, de lumière, de son, de mouvement, des actionneurs de LED, un écran LCD, un module de relais et un module de boutons-poussoirs. Tous ces composants sont équipés de connecteurs standard Grove, ce qui permet une connexion facile et rapide à la carte GrovePi Zero sans soudure ni câblage.

Le kit GrovePi Zero est destiné aux débutants et aux utilisateurs avancés qui souhaitent explorer les capacités du Raspberry Pi Zero dans les projets de l'IoT, de la domotique, de la robotique ou de l'automatisation. En utilisant le langage de programmation Python, il est facile de programmer les différents capteurs et actionneurs du kit GrovePi Zero pour réaliser des projets innovants et créatifs. 

## Pratique

### Git
- Effectuez un fork de ce dépôt

### GrovePi
- Connectez le potentiomètre sur le port A0
- Connectez le bouton sur le port A1
- Connectez une LED sur le port D1
- Connecter l’écran LCD sur le port I2C

![grovepi](img/image3.jpg)

## Win32DiskImager

Win32DiskImager est un logiciel open source pour Windows, qui permet de copier une image disque complète sur une carte SD ou une clé USB. Il est principalement utilisé pour la création de cartes SD bootables pour les mini-ordinateurs Raspberry Pi.

L'utilité de Win32DiskImager pour le Raspberry Pi est de permettre de créer une carte SD bootable contenant un système d'exploitation pour le Raspberry Pi. En effet, les mini-ordinateurs Raspberry Pi n'ont pas de stockage interne, et nécessitent une carte SD pour exécuter un système d'exploitation. En utilisant Win32DiskImager, il est facile de copier une image disque du système d'exploitation sur une carte SD, de sorte que le Raspberry Pi puisse démarrer depuis la carte SD et exécuter le système d'exploitation.

En plus de la création de cartes SD bootables, Win32DiskImager peut également être utilisé pour sauvegarder ou cloner des cartes SD ou des clés USB, ce qui est utile pour les projets qui nécessitent de multiples copies d'une même carte SD ou clé USB.

- Installez et lancez Win32DiskImager
- Flashez l’image GrovePi sur la carte micro SD
- Insérez la carte micro SD dans le Raspberry Pi Zero W et démarrez-le

## Putty

PuTTY est un logiciel libre et gratuit pour Windows qui permet d'établir une connexion sécurisée SSH (Secure Shell), Telnet, Rlogin ou SFTP avec un serveur distant. PuTTY est un client qui permet d'établir une session de terminal sur une machine distante, permettant ainsi d'interagir avec elle comme si l'on était physiquement sur cette machine.

En ce qui concerne le Raspberry Pi, PuTTY peut être utilisé pour se connecter en toute sécurité à distance à un Raspberry Pi à partir d'un ordinateur exécutant Windows. Lorsqu'un Raspberry Pi est configuré pour accepter les connexions SSH, il est possible de se connecter à celui-ci à partir d'un ordinateur Windows à l'aide de PuTTY.

Une fois connecté, l'utilisateur peut administrer et contrôler le Raspberry Pi à distance, en exécutant des commandes à partir d'un terminal, en transférant des fichiers vers ou depuis le Raspberry Pi, en modifiant des fichiers de configuration, en accédant à des services Web hébergés sur le Raspberry Pi, ou encore en surveillant les journaux du système.

PuTTY est particulièrement utile pour les utilisateurs qui ne sont pas familiers avec les commandes en ligne de commande de Linux, car il permet d'interagir avec le Raspberry Pi de manière graphique et intuitive.

- Récupérez l’adresse IP du Raspberry Pi (elle s’affiche sur l’écran LCD)
    ```bash
    192.168.0.214
    ```
- Connectez-vous en SSH à Putty
- Clonez avec git le dépot que vous avez *fork*
    ```bash
    git clone
    ```
- Ouvrez une invite de commande Python et exécutez les instructions suivantes :
    ```python
    from grovepi import *
    pinMode(3, "OUTPUT")
    digitalWrite(3,1)
    ```
- Que se passe-t-il ?
    > 1  
    > 1
- Dans la même console Python et exécutez l’instruction suivante :
    ```python
    digitalWrite(3,0)
    ```
- Que se passe-t-il ?
    > 1
- Dans la même console Python et exécutez les instructions suivantes :
    ```python
    pinMode(1, "INPUT")
    analogRead(1)
    ```
- Quel est le résultat affiché ?
    > 1  
    > 0
- Pressez sur le bouton et exécutez l’instruction suivante :
    ```python
    analogRead(1)
    ```
- Quel est le résultat affiché ?
    > 1023

### FileZilla
FileZilla est un logiciel libre et gratuit de transfert de fichiers, disponible pour Windows, Linux et macOS. Il permet de transférer des fichiers entre un ordinateur local et un serveur distant via les protocoles FTP, FTPS et SFTP.

En ce qui concerne le Raspberry Pi, FileZilla peut être utilisé pour transférer des fichiers entre un ordinateur local et le Raspberry Pi via une connexion SFTP (Secure File Transfer Protocol). Lorsqu'un serveur SSH est configuré sur le Raspberry Pi, il est possible d'utiliser FileZilla pour transférer des fichiers entre l'ordinateur local et le Raspberry Pi en toute sécurité.

L'utilité de FileZilla pour le Raspberry Pi est qu'il facilite le transfert de fichiers entre l'ordinateur local et le Raspberry Pi. Il permet de copier des fichiers vers ou depuis le Raspberry Pi, d'éditer des fichiers de configuration directement sur le Raspberry Pi ou encore de sauvegarder des données importantes.

FileZilla est particulièrement utile pour les utilisateurs qui ne sont pas familiers avec les commandes en ligne de commande de Linux, car il permet d'interagir avec le Raspberry Pi de manière graphique et intuitive. Il est également utile pour les utilisateurs qui ont besoin de transférer de nombreux fichiers ou de gros fichiers entre l'ordinateur local et le Raspberry Pi, car il facilite grandement le processus de transfert.

### Lancez FileZilla
Connectez-vous au Raspberry Pi
- Connectez-vous au Raspberry Pi avec FileZilla
- Créez un dossier scripts sur votre SSD à l’emplacement de votre choix
- Ouvrez le dépôt et votre dossier face à face dans FileZilla
- Récupérez le fichier `blinky.py`
- Ouvrez le dossier de votre SSD avec VS Code
- Ajoutez les lignes suivantes à `blink.py` :
    ```python
    digitalWrite(3,1)
    time.sleep(1)
    digitalWrite(3,0)
    ```
- Avec FileZilla, synchronisez blink.py avec le dossier distant
- Exécutez le script sur le Raspberry Pi
- Que s’est-t-il passé ?
    > La lumière s'allume pendant 1 seconde puis s'éteint
- Indique ce que fait chaque ligne dans le fichier `blinky.py`





