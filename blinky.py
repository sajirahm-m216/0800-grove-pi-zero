#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : blink.py
#
# Auteur (Nom, Prénom)               : Jérémy Michaud
# Classe                             : 
# Module                             : M216
# Date de création                   : 26.02.2024
#
# Description succincte du programme :
#   TODO décrire le focntionnement du script
# ----------------------------------------------------------------------------

from grovepi import *   # Importe la librairie grovepi pour gérer le raspberry
import time             # Importe la librairie pour gérer les fonctions liées au temps


pinMode(3, "OUTPUT")    # Définit le pin 3 en tant que sortie
digitalWrite(3,1)       # Définit le pin 3 comme allumé
time.sleep(1)           # Attend une seconde
digitalWrite(3,0)       # Définit le pin 3 comme éteint
